package org.hepeng.compression.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompressionTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompressionTestApplication.class, args);
    }

}
